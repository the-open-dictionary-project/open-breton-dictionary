package cymru.prv.dictionary.breton.tenses;

import cymru.prv.dictionary.breton.BretonVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;


/**
 * Represents the imperfect tense in Breton
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class ImperfectBretonVerbTense extends BretonVerbTense {

    public ImperfectBretonVerbTense(BretonVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.singletonList(apply("en"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(apply("es"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return Collections.singletonList(apply("e"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(apply("emp"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Collections.singletonList(apply("ec'h"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Collections.singletonList(apply("ent"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(apply("ed"));
    }
}
