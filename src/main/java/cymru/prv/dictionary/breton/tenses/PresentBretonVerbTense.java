package cymru.prv.dictionary.breton.tenses;

import cymru.prv.dictionary.breton.BretonVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;


/**
 * Represents the present tense in Breton
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class PresentBretonVerbTense extends BretonVerbTense {

    public PresentBretonVerbTense(BretonVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.singletonList(apply("an"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(apply("ez"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return Collections.singletonList(apply(""));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(apply("omp"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Collections.singletonList(apply("it"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Collections.singletonList(apply("ont"));
    }

    @Override
    protected List<String> getDefaultImpersonal() {
        return Collections.singletonList(apply("er"));
    }
}
