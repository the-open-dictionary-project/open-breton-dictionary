package cymru.prv.dictionary.breton;

/**
 * An interface for number to text converters
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public interface ConvertNumberToText {
    String convertToText(long number);
}
