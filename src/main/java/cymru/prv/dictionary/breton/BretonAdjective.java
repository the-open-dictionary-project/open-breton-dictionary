package cymru.prv.dictionary.breton;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.WordType;
import cymru.prv.dictionary.common.json.Json;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;


/**
 * Represents a Breton adjective
 * <p>
 * In addition to the information as other words
 * it contains information about the equative,
 * comparative, superlative, and exclamative
 * versions of the word.
 * </p>
 * <p>
 * All of these will be generated.
 * </p>
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class BretonAdjective extends BretonWord {

    private static final String EQUATIVE = "equative";
    private static final String COMPARATIVE = "comparative";
    private static final String SUPERLATIVE = "superlative";
    private static final String EXCLAMATIVE = "exclamative";

    private final String stem;
    private final List<String> exclamative;
    private final List<String> equative;
    private final List<String> comparative;
    private final List<String> superlative;


    /**
     * Creates an adjective based on the data in obj.
     *
     * @param obj the data for the word
     */
    public BretonAdjective(JSONObject obj) {
        super(obj, WordType.adjective);
        stem = obj.optString("stem", getStem(getNormalForm()));
        equative = obj.has(EQUATIVE) ? Json.getStringList(obj, EQUATIVE) : getEquative();
        comparative = obj.has(COMPARATIVE) ? Json.getStringList(obj, COMPARATIVE) : getComparative();
        superlative = obj.has(SUPERLATIVE) ? Json.getStringList(obj, SUPERLATIVE) : getSuperlative();
        exclamative = obj.has(EXCLAMATIVE) ? Json.getStringList(obj, EXCLAMATIVE) : getExclamative();
    }

    private String getStem(String normalForm) {
        // Create stem
        return normalForm.replaceFirst("(a|i|o|u|an|añ|at|ein|ed|eg|et|in|iñ|yd)$", "")
                // provecation
                .replaceFirst("b$", "p")
                .replaceFirst("z$", "s");
    }

    private List<String> getEquative() {
        return Collections.singletonList("ken " + getNormalForm());
    }

    private List<String> getComparative() {
        return Collections.singletonList(stem + "oc'h");
    }

    protected List<String> getSuperlative() {
        return Collections.singletonList(stem + "añ");
    }

    private List<String> getExclamative(){
        return Collections.singletonList(stem + "at");
    }

    @Override
    protected JSONObject getInflections() {
        JSONObject obj = new JSONObject();
        obj.put(EQUATIVE, equative);
        obj.put(COMPARATIVE, comparative);
        obj.put(SUPERLATIVE, superlative);
        obj.put(EXCLAMATIVE, exclamative);
        return obj;
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        list.addAll(equative);
        list.addAll(comparative);
        list.addAll(superlative);
        list.addAll(exclamative);
        return list;
    }
}
