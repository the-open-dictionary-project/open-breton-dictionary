package cymru.prv.dictionary.breton;

import cymru.prv.dictionary.breton.tenses.*;
import cymru.prv.dictionary.common.Conjugation;
import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.WordType;
import cymru.prv.dictionary.common.json.Json;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;


/**
 * Represents a Breton verb
 * <p>
 * In addition to the information as other words
 * it contains information about different conjugations
 * and tenses for the verb.
 * </p>
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class BretonVerb extends BretonWord {

    private static final String PAST_PARTICIPLE = "past_participle";

    private final String stem;
    private final List<String> pastParticiples;
    private final Map<String, Conjugation> tenses = new HashMap<>();

    /**
     * Reads data from the JSON object
     * and fills out the necessary fields.
     *
     * @param obj  a JSON object containing at least
     *             the field "normalForm". The field
     *             "notes" is optional
     */
    public BretonVerb(JSONObject obj) {
        super(obj, WordType.verb);
        stem = obj.optString("stem", getStem(getNormalForm()));
        if(obj.has(PAST_PARTICIPLE))
            pastParticiples = Json.getStringList(obj,"past_participle");
        else
            pastParticiples = List.of(stem + "et");

        if(!obj.optBoolean("conjugates", true))
            return;

        // Will always be added
        addRequiredTense(obj, "present", PresentBretonVerbTense::new);
        addRequiredTense(obj, "imperfect", ImperfectBretonVerbTense::new);
        addRequiredTense(obj, "preterite", PreteriteBretonVerbTense::new);
        addRequiredTense(obj, "future", FutureBretonVerbTense::new);
        addRequiredTense(obj, "conditional_present", ConditionalPresentBretonVerbTense::new);
        addRequiredTense(obj, "conditional_imperfect", CondtitionalImperfectBretonVerbTense::new);
        addRequiredTense(obj, "imperative", ImperativeBretonVerbTense::new);

        // Will only be added / generated if it is present
        addOptionalTense(obj, "present_habitual", PresentBretonVerbTense::new);
        addOptionalTense(obj, "present_situative", (verb, o) -> new Conjugation(o));
        addOptionalTense(obj, "imperfect_habitual", ImperfectBretonVerbTense::new);
        addOptionalTense(obj, "imperfect_situative", (verb, o) -> new Conjugation(o));
    }

    private String getStem(String normalForm) {
        return normalForm.replaceFirst("(a|i|o|u|an|añ|at|ein|ed|eg|et|out|in|iñ|yd)$", "");
    }

    @Override
    public JSONObject getMutations() {
        return BretonMutation.getMutations(getNormalForm());
    }

    @Override
    public JSONObject toJson() {
        JSONObject obj = super.toJson();
        if(pastParticiples != null)
            obj.put("past_participle", pastParticiples);
        return obj;
    }

    @Override
    protected JSONObject getConjugations() {
        if(tenses.size() == 0)
            return null;
        JSONObject obj =  new JSONObject();
        for(String key : tenses.keySet())
            obj.put(key, tenses.get(key).toJson());
        return obj;
    }

    protected void addOptionalTense(JSONObject obj, String tense,  BiFunction<BretonVerb, JSONObject, Conjugation> constructor){
        if(obj.has(tense)){
            Conjugation tenseObj = constructor.apply(this, obj.getJSONObject(tense));
            if(tenseObj.has())
                tenses.put(tense, tenseObj);
        }
    }

    protected void addRequiredTense(JSONObject obj, String tense, BiFunction<BretonVerb, JSONObject, Conjugation> constructor){
        if(obj.has(tense)){
            Conjugation tenseObj = constructor.apply(this, obj.getJSONObject(tense));
            if(tenseObj.has())
                tenses.put(tense, tenseObj);
        }
        else {
            tenses.put(tense, constructor.apply(this, new JSONObject("{}")));
        }
    }

    public String getStem() {
        return stem;
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        if(pastParticiples != null)
            list.addAll(pastParticiples);
        for(Conjugation c : tenses.values())
            list.addAll(c.getVersions());
        return list;
    }
}
