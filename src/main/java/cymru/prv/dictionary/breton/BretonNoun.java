package cymru.prv.dictionary.breton;

import cymru.prv.dictionary.breton.BretonWord;
import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.WordType;
import cymru.prv.dictionary.common.json.Json;
import org.json.JSONObject;

import java.util.List;

/**
 * Represents a Breton noun
 * <p>
 * In addition to the normal data it contains
 * information about the plural of the word if
 * it exists.
 * </p>
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class BretonNoun extends BretonWord {

    private static final String PLURAL = "plural";

    private final List<String> plurals;

    /**
     * Creates an instance of the word.
     * The field normalForm is required and can not be empty.
     *
     * @param obj        The data for the word
     * @throws IllegalArgumentException if normalForm is empty or null
     */
    public BretonNoun(JSONObject obj) {
        super(obj, WordType.noun);
        plurals = Json.getStringListOrNull(obj, PLURAL);
    }

    @Override
    protected JSONObject getInflections() {
        if(plurals != null)
            return new JSONObject().put(PLURAL, plurals);
        return null;
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        if(plurals != null)
            list.addAll(plurals);
        return list;
    }
}
