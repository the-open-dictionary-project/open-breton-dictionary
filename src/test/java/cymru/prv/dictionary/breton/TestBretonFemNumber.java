package cymru.prv.dictionary.breton;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestBretonFemNumber {

    @Test
    public void test2(){
        Assertions.assertEquals(
                "div",
                new BretonFemNumberToText().convertToText(2)
        );
    }

    @Test
    public void test3(){
        Assertions.assertEquals(
                "teir",
                new BretonFemNumberToText().convertToText(3)
        );
    }

    public void test4(){
        Assertions.assertEquals(
                "peder",
                new BretonFemNumberToText().convertToText(4)
        );
    }

    public void test402(){
        Assertions.assertEquals(
                "pevar kant div",
                new BretonFemNumberToText().convertToText(402)
        );
    }

}
